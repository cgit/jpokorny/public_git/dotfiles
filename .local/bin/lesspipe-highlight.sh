#!/bin/bash
# TODO: cache source-highlight extensions?
# TODO: redo as ~/.lessfilter,
#       see https://bugzilla.redhat.com/show_bug.cgi?id=885122

# XXX: enable overriding
LESSPIPE="$(which lesspipe.sh 2>/dev/null)"
SHLPIPE="$(which src-hilite-lesspipe.sh 2>/dev/null)"

SHLEXTS=

[ -x "${LESSPIPE}" ] || LESSPIPE=cat
[ -x "${SHLPIPE}" ] \
  && SHLEXTS=$(source-highlight --lang-list | cut -d = -f1 | sed 's/^/\*./' | paste -d'|' -s) \
  || SHLPIPE=cat

eval "\
	case \"$1\" in
	${SHLEXTS%|}) ${SHLPIPE} "$1";;
	*)          ${LESSPIPE} "$1";;
	esac"
