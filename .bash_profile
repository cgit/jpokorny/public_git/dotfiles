# .bash_profile

# workaround for ~/.profile not being sourced in tmux env.
if [ -z "${_LOCAL_PROFILE}" ]; then
	. ~/.profile
fi

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
source ~/.local/bin/history-backup
