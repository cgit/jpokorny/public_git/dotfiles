#!/bin/bash

STARTDIR=.
if [ $# -ge 1 ]; then
	STARTDIR="$1"
fi

find "${STARTDIR}" -name '*.xml' -and -not -empty -print0 \
  | xargs -0 -I "{}" \
    sh -c 'mv "{}" "{}.orig"; xsltproc identity_modulo_mtime.xsl "{}.orig" > "{}"; rm -- "{}.orig"'
