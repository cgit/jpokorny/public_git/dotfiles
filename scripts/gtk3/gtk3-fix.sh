#!/bin/bash
# credit:
# http://tacticalvim.wordpress.com/2013/01/05/using-the-adwaita-gtk3-theme-with-xfce-and-lightdm/
#
# to be run either as a root (system-wide fix only) or as a user
# (user-specific customization, but prompts for root password to
# do the main task)
#
# gtk-fallback.css notes:
# - https://bugs.archlinux.org/task/34002
# - https://git.gnome.org/browse/gnome-themes-standard/commit/?id=1aed68dbda8b93b39d2586df132cc14ca337c4d9

THEME=theme-adwaita
test $# -gt 0 && THEME=$1
source "${THEME}"

GR_BASEDIR="${THEME_DIR}/gtk-3.0"
GR_FILE="${THEME_FILE:-${GR_BASEDIR}/gtk.gresource}"
GR_BASEROOT="${THEME_BASEROOT}"
GR_FILES="${GR_BASEDIR}/files"

if test $(id -u) -ne 0; then
	echo "need to rerun this script as root"
	su -c "$0 $@" || exit $?
	# run customization if defined
	type -t theme_customize | grep -q function \
	  && theme_customize "${GR_FILES}"
else
	mkdir "${GR_FILES}" || exit  # sort of self-defense
	pushd "${GR_FILES}" >/dev/null || exit
	for RSRC in $(gresource list $GR_FILE); do
		RSRC_FILE="$(echo "${RSRC#$GR_BASEROOT}")"
		mkdir -p "$(dirname "$RSRC_FILE")" ||:
		gresource extract "$GR_FILE" "$RSRC" > "$RSRC_FILE"
	done
	popd >/dev/null
	# absolutize imports
	find "${GR_BASEDIR}" -name '*.css' -print0 | xargs -0 -I '{}' \
	  sed -i.bck \
	    -e "s|^\(@import\s\+url\s*(\s*\"\)\(resource://${GR_BASEROOT}\)\?\(.*\)|\1file:///${GR_FILES}/\3|" \
	  '{}'
	exit $?
fi
