#!/bin/bash

WHICH='ssh'  # pipe-delimited
WHERE='~/.ssh'  # space delimited

SEHOMEPOLICY='/etc/selinux/targeted/contexts/files/file_contexts.homedirs'

TMPFILE=$(mktemp /tmp/.XXXXX)
#SETFILESCMD="setfiles -n -s -v -o - ${TMPFILE}"  # dry-run
SETFILESCMD="setfiles -s -v -o - ${TMPFILE}"


# prologue: unrelated to SELinux, but affects password-less ssh in the same way
chmod 700 -- ~/.ssh
CURDIR="$(dirname $(readlink -f ~/.ssh))"
HOMEINODE=$(stat --printf %i ~)
while [ $(stat --printf %i "${CURDIR}") -ne "${HOMEINODE}" ]; do
	chmod 700 -- "${CURDIR}"
	CURDIR="$(dirname ${CURDIR})"
done

grep -E "${WHICH}" -- "${SEHOMEPOLICY}" >>${TMPFILE}
${SETFILESCMD} < \
  <(echo "${WHERE}" | xargs -I '{}' sh -c '[ -d {} ] && echo "{}/"; echo "{}"')
rm -f -- "${TMPFILE}"
