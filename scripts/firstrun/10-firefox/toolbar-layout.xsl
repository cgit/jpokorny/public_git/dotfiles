<!--
  TODO: increase modularity and possibility to predefine more + order
  -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <!-- xsl:template match="@currentset[../RDF:Description[]]" -->
  <xsl:template match="@currentset[
                         name(..)='RDF:Description'
                         and
                         ../@RDF:about='chrome://browser/content/browser.xul#addon-bar'
                       ]">
    <xsl:attribute name="currentset">
      <!-- TODO: check if not empty or there already -->
      <xsl:value-of select="concat(., '|feed-button')"/>
    </xsl:attribute>
  </xsl:template>
</xsl:stylesheet>
