#!/bin/bash

# see misc/crontab
# the script suppose ./folders file with one folder per-line,
# prefixed with '=' and ending with '\'
#
# TODO: comparison with https://gitorious.org/chrisirwin-utils/maildir-indicator

: ${BLACKLIST:="__DUMMY__"}

wget https://raw.github.com/erikw/dotfiles/personal/bin/maildir_watch.sh -O- \
  | sed \
    -e "s|\(maildir_path=\"\)[^\"]*\"|\1$(
      grep -E 'set folder\s+=' ../.muttrc | tr -d ' ' | cut -d= -f2 | sed 's|\~|$HOME|'
    )\"|" \
    -e "s|\(mailboxes=(\)[^)]*)|\1$(
      grep -E '=[A-Za-z0-9.-]+\s.*\\' folders | grep -Ev "${BLACKLIST}" \
        | sed 's|^=\([A-Za-z0-9.-]\+\)\s.*$|\1|' | paste -s
      ))|" > maildir_watch.sh
chmod +x maildir_watch.sh
