"set tabstop=4
"set shiftwidth=4
"set expandtab
set number
set incsearch
set undofile

set mouse=a
" see https://bugzilla.redhat.com/show_bug.cgi?id=952758
"set ttym=sgr
set ttym=xterm2  " make buffer mouse dragging work also in tmux

"set autoindent
"set vb
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE

noremap <C-left> :bprev<CR>
noremap <C-right> :bnext<CR>
" alternatively:
" http://stackoverflow.com/a/12230039
"set hidden
"noremap <C-TAB> :bnext<CR>
"noremap <C-S-TAB> :bprev<CR>

" omnicompletion to C-space?
"inoremap <C-Space> <C-x><C-o>
"inoremap <C-@> <C-Space>

execute pathogen#infect()
execute pathogen#infect('~/.vim/vim4projects/{}')
":Helptags

syntax on
filetype plugin indent on

" NERDTree
let NERDTreeIgnore = ['\.pyc$', '\.tar\.gz$']

" powerline
set laststatus=2  " Always show the statusline
set noshowmode  " Remove -- INSERT -- etc. redundancy
set t_Co=256  " Explicitly tell vim that the terminal supports 256 colors

" tagbar
nmap <F8> :TagbarToggle<CR>
let NERDTreeIgnore = ['\.pyc$', '\.tar\.gz$']

"
" others
"
if &diff
  " pass setting shell for vimdiff as it caused auto-stop for some reason
else
  " normal mode
  set shell=/bin/bash\ -i
endif

" http://vim.wikia.com/wiki/Always_start_on_first_line_of_git_commit_message
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
