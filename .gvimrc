"colorscheme desert
colorscheme mod_tcsoft
highlight LineNr gui=NONE guifg=DarkGrey guibg=NONE

set guifont=Inconsolata\ Medium\ 11
noremap <F12> :let &guifont = substitute(&guifont, '\(\D*\)\(\d\+\)\(\D*\)', '\=submatch(1).(submatch(2)+1)', '')<CR>
noremap <S-F12> :let &guifont = substitute(&guifont, '\(\D*\)\(\d\+\)\(\D*\)', '\=submatch(1).(submatch(2)-1)', '')<CR>

"set guioptions+=b
set nowrap

set lines=60
set columns=104

set showtabline=-0

"make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=e  "remove tab bar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
" toggle menu
nnoremap <C-F1> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
